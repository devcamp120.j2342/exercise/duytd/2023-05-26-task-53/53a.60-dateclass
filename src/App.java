import models.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date(29, 12, 1996);
        Date date2 = new Date(1, 1, 2020);

        System.out.println(date1);
        System.out.println(date2);
    }
}
